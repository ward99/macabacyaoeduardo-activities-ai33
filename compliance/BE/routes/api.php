<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\WorkerController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\LoginController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/available', [StatusController::class, 'available']);
Route::get('/timeIn', [StatusController::class, 'timeIn']);
Route::get('/timeOut', [StatusController::class, 'timeOut']);
Route::get('/scheduleChart',[StatusController::class, 'chartData']);

Route::resources([
    'workers' => WorkerController::class,
    'locations' => LocationController::class,
    'admin' => AdminController::class
]);

Route::middleware('auth:sanctum')->group(function () {
    Route::post('/logout', [LoginController::class, 'logout']);
});

Route::get('user', [LoginController::class, 'user']);
Route::post('/register', [LoginController::class, 'register']);
Route::post('/login', [LoginController::class, 'authenticate']);



