<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Worker extends Authenticatable
{
    use HasFactory,HasApiTokens;

    protected $table = 'workers';
    protected $fillable = ['first_name','last_name','middle_name','age','password'];


    public function admin(){
        return $this->hasMany(Admin::class);
    }
}
