<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use HasFactory;
    
    protected $table = 'admin';
    protected $fillable =['worker_id','schedule_from','schedule_to','location_id','status'];

    public function worker(){
        return $this->belongsTo(Worker::class);
    }

    public function location(){
        return $this->belongsTo(Location::class);
    }

}
