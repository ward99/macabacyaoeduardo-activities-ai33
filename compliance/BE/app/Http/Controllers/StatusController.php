<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Admin;
use App\Models\Location;

class StatusController extends Controller
{
    
    public function available()
    {
        $timeIn = Admin::with('location','worker')->where('status', 'Assigned')->orderBy('worker_id')->get();

        return response()->json($timeIn);
    }

    public function timeIn()
    {
        $timeIn = Admin::with('location','worker')->where('status', 'Time-In')->orderBy('worker_id')->get();

        return response()->json($timeIn);
    }

    public function timeOut()
    {
        $timeOut = Admin::with('location','worker')->where('status', 'Time-Out')->orderBy('worker_id')->get();

        return response()->json($timeOut);
    }

    public function chartData()
    {
        $data = Location::with('admin')->get();

        return response()->json($data);

    }
}
