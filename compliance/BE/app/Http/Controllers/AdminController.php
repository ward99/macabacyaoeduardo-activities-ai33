<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Http\Requests\AdminRequest;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = Admin::with('worker','location')->get();
        $timeOut = Admin::with('location','worker')->where('status', 'Time-Out')->orderBy('worker_id')->get();

        return response()->json($admin);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {
        
        $validated = $request->validated();

        $admin = new Admin;

        $admin->worker_id = $request->input('worker_id');
        $admin->location_id = $request->input('location_id') ;
        $admin->status = $request->input('status');
        $admin->schedule_from = $request->input('schedule_from');
        $admin->schedule_to = $request->input('schedule_to');

        $admin->save();

        return response()->json($admin);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminRequest $request, $id)
    {
        $validated = $request->validated();

        $admin = Admin::find($id);

        $admin->worker_id = $request->input('worker_id');
        $admin->location_id = $request->input('location_id') ;
        $admin->status = $request->input('status');
        $admin->schedule_from = $request->input('schedule_from');
        $admin->schedule_to = $request->input('schedule_to');

        $admin->save();

        return response()->json($admin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = Admin::find($id);
        $admin->delete();

        return response()->json($admin);
    }
}
