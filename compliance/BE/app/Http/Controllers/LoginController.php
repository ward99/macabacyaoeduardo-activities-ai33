<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Cookie;


class LoginController extends Controller
{   

    public function register(Request $request){

        $workers = new User;

        $workers->name = $request->input('name');
        $workers->email = $request->input('email');
        $workers->password = Hash::make($request->input('password'));

        $workers->save();

        return ["message" => $workers];
    }

    public function authenticate(Request $request){
        
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => ['required'],
        ]);

        if(!Auth::attempt($credentials)){

            return response(['message' => 'Unauthenticated']);
            
        }
        
        $token = $request->user()->createToken('token')->plainTextToken;
        $cookie = cookie('jwt', $token, 60*24);
        
        return response(["message" => $token, "error" => Auth::user()])->cookie($cookie);
    }

    public function logout(Request $request){
        return response('Success')->withoutCookie('jwt');
    }

    public function user(){

        return Auth::user();
    }
}
