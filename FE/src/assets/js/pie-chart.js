import { Pie } from 'vue-chartjs'

export default {
  extends: Pie,
  mounted () {
    this.renderChart({
      labels: ['Patron', 'Popular books'],
      datasets: [
        {
          backgroundColor: ['#15647D','#B72424' ],
          borderColor: '#9DBCC7',
          borderWidth: 1,
          data:  [21,3],
          padding:5
        }
      ]
    }, {responsive: true, maintainAspectRatio: false})
  },
}
