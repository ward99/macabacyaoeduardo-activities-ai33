import { Bar } from 'vue-chartjs' 


export default {
  extends: Bar,
  mounted () {
    this.renderChart({
      labels: ['Borrowed', 'Returned'],
      datasets: [
        {
          label:'Borrowed And Returned',
          backgroundColor:['#15647D','#B72424'],
          data: [24,16,0],
          borderColor: '#9DBCC7',
          borderWidth: 2,
          padding:5
        }
      ],
    }, {responsive: true, maintainAspectRatio: false})
  }
}
