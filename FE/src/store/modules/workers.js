import axios from 'axios'



export default {
    namespaced:true,
    state: () => ({
        
        booksData:[],
        categories:[]

    }),
    mutations:{

       
    },
    actions:{

        gew(){
            axios.get('http://127.0.0.1:8000/api/locations')
            .then(
                res => {
                    console.log(res.data)
                }
            )
        },

        getLocation()
        {
            axios.post('http://127.0.0.1:8000/api/locations',{
                location: ""
            })
            .then(res => {
                console.log(res.data)
             })
            .catch(error => {
                console.log(error.response.data.errors);
             });
        },

       
    },
    getters:{

        data(state)
        {
            return state.booksData.reverse()
        },

        categories(state)
        {
            return state.categories
        }
    }
}
