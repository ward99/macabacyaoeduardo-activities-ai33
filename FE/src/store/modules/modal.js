
const modal = {
    namespaced:true,
    state: () => ({

        Data: null,
        modalsTarget: null,
        
    }),

    mutations: {

        GET_DATA(state, data )
        {
            state.Data = data
        },

        GET_MODALS_TARGET(state, payload)
        {
            state.modalsTarget = payload
        }

    },

    actions:{

        modalActions({commit}, modalsPayload)
        {
            if(modalsPayload.modalTarget == "addModal")
            {
                commit('GET_MODALS_TARGET', modalsPayload.modalTarget)
            }

            if(modalsPayload.modalTarget == "updateModal")
            {
                commit('GET_MODALS_TARGET', modalsPayload.modalTarget) 
                commit('GET_DATA', modalsPayload.data)
            }

            if(modalsPayload.modalTarget == "deleteModal")
            {
                commit('GET_MODALS_TARGET', modalsPayload.modalTarget)
                commit('GET_DATA', modalsPayload.data)
            }

            if(modalsPayload.modalTarget == "borrowModal")
            {
                commit('GET_MODALS_TARGET', modalsPayload.modalTarget)
                commit('GET_DATA', modalsPayload.data)
            }

            if(modalsPayload.modalTarget == "returnModal")
            {
                commit('GET_MODALS_TARGET', modalsPayload.modalTarget)
                commit('GET_DATA', modalsPayload.data)
            }
        }

    },

    getters:{
        
        modalTarget(state)
        {
            return state.modalsTarget  
        },

        data(state)
        {
            return state.Data  
        }


    }
}

export default modal