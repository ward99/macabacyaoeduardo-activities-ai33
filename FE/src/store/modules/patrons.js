import axios from 'axios'

const patron = {
    namespaced:true,
    state: () => ({

        patronsData:[],

    }),
    mutations:{

        SET_PATRONS_DATA(state, data)
        {
            state.patronsData =  data
        },

    },
    actions:{

        getPatronsData({commit})
        {
            axios.get('http://127.0.0.1:8000/api/patrons')
            .then(res => {
                commit('SET_PATRONS_DATA' , res.data)
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        addPatronsData({dispatch},patrons)
        {
            axios.post('http://127.0.0.1:8000/api/patrons', patrons)
            .then(res => {
                dispatch('getPatronsData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        updatePatronsData({dispatch},patrons)
        {
            axios.put(`http://127.0.0.1:8000/api/patrons/${patrons.id}`, patrons)
            .then(res => {
                dispatch('getPatronsData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        deletePatronsData({dispatch},patrons)
        {
            axios.delete(`http://127.0.0.1:8000/api/patrons/${patrons.id}`)
            .then( (res) => {
                dispatch('getPatronsData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        }

    },
    getters:{
        
        data(state)
        {   
            return state.patronsData.reverse()
        }

    }

}

export default patron