import axios from 'axios'

export default {
    namespaced:true,
    state: () => ({
        data:[],

    }),
    mutations:{

        LOCATIONS_DATA(state,data){
            state.locationsData = data
        },
       
    },
    actions:{

        getLocation({commit}){
            axios.get('http://127.0.0.1:8000/api/locations')
            .then(res => 
                {
                    commit('LOCATIONS_DATA', res.data)
                    console.log(res.data)
                }
            )
        },

        postLocation({dispatch}, data)
        {
            axios.post('http://127.0.0.1:8000/api/locations', data)
            .then(res => {
                dispatch('getLocation')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error.response.data.errors);
             });
        },

        putLocation({dispatch},data)
        {
            axios.put(`http://127.0.0.1:8000/api/locations/${data.id}`, data.location)
            .then(res => {
                dispatch('getLocation')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error.response.data.errors);
             });
        },

        deleteLocation({dispatch},data)
        {
            axios.delete(`http://127.0.0.1:8000/api/locations/${data}`)
            .then(res => {
                dispatch('getLocation')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error.response.data.errors);
             });
        },

       
    },
    getters:{

        data(state)
        {
            return state.locationsData
        },

    }
}
