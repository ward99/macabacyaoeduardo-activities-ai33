import axios from 'axios'

const books = {
    namespaced:true,
    state: () => ({
        
        booksData:[],
        categories:[]

    }),
    mutations:{

        SET_BOOKS_DATA(state,payload)
        {
            state.booksData = payload
        },

        SET_CATEGORIES_DATA(state,payload)
        {
            state.categories = payload
        }

    },
    actions:{

        getCategoriesData({commit})
        {
            axios.get('http://127.0.0.1:8000/api/categories')
            .then(res => {
                commit('SET_CATEGORIES_DATA' , res.data)
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        getBooksData({commit})
        {
            axios.get('http://127.0.0.1:8000/api/books')
            .then(res => {
                commit('SET_BOOKS_DATA' , res.data)
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        addBooksData({dispatch},books)
        {
            axios.post('http://127.0.0.1:8000/api/books', books)
            .then(res => {
                dispatch('getBooksData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        updateBooksData({dispatch}, books)
        {
            axios.put(`http://127.0.0.1:8000/api/books/${books.id}`, books)
            .then(res => {
                dispatch('getBooksData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        deleteBooksData({dispatch},books)
        {
            axios.delete(`http://127.0.0.1:8000/api/books/${books.id}`)
            .then( (res) => {
                dispatch('getBooksData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },
    },
    getters:{

        data(state)
        {
            return state.booksData.reverse()
        },

        categories(state)
        {
            return state.categories
        }
    }
}

export default books