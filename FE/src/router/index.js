import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login'
import Dashboard from '../components/Dashboard'
import Workers from '../components/Workers'
import Locations from '../components/Locations'
import Schedules from '../components/Schedules'
import User from '../components/User'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/User',
    name: 'User',
    component: User
  },
  {
    path: '/Dashboard',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/Schedules',
    name: 'Schedules',
    component: Schedules
  },
  {
    path: '/Workers',
    name: 'Workers',
    component: Workers
  },
  {
    path: '/Locations',
    name: 'Locations',
    component: Locations
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
