<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\BookController;
use App\Http\Controllers\PatronController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\BorrowedBooksController;
use App\Http\Controllers\ReturnedBooksController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//book and patron
Route::resources([
    'books' => BookController::class,
    'patrons' => PatronController::class
]);

//categories
Route::get('categories',[CategoryController::class, 'index']);

//borrowedBook
Route::post('books/borrowed_books',[BorrowedBooksController::class, 'store']);

//returnedBook
Route::post('books/returned_books',[ReturnedBooksController::class, 'store']);