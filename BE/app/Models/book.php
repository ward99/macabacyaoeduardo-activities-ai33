<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory; 
    
    protected $table = 'books';
    protected $fillable = ['name','author','copies' ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function borrowedbook()
    {
        return $this->hasMany(BorrowedBook::class);
    }

    public function returnedbooked()
    {
        return $this->hasMany(ReturnedBook::class);
    }

}
