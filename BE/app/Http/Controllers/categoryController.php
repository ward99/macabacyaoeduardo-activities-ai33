<?php

namespace App\Http\Controllers;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index(Request $request)
    {
        $categories = Category::all();
        return response()->json($categories);
    }


    public function show($id)
    {
        $book = Category::find($id)->book;
        return response()->json($book);
    }
}
