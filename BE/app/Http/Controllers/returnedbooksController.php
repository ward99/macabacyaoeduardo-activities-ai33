<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CopiesValidation;
use App\Models\ReturnedBook;
use App\Models\BorrowedBook;

class ReturnedBooksController extends Controller
{
    public function store(CopiesValidation $request)
    {
        $borrowed = BorrowedBook::where('copies')->get();
        
        $return = new ReturnedBook;
        $return->copies = $request->input('copies');

        if(intval($return->copies) > $borrowed){

            return abort(404);
        }

        else{

            $returned_copies = intval($return->copies) + intval($borrowed);
            $return->copies = $returned_copies;
            $return->save();
            return response()->json($return); 
             
        }
    }
}
