<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CopiesValidation;
use App\Models\BorrowedBook;
use App\Models\Book;

class BorrowedBooksController extends Controller
{
    public function store(CopiesValidation $request)
    {   

        $book = Book::where('copies')->get();
        
        $borrowed = new BorrowedBook;
        $borrowed->copies = $request->input('copies');

        if(intval($borrowed->copies) > $book){

            return abort(404);
        }

        else{

            $borrowed->save();
            return response()->json($borrowed); 
             
        }

       
    }
}
