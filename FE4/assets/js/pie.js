var ctx = document.getElementById('bar').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Borrowed', 'Returned'],
        datasets: [{
            label: 'Borrowed and Returned Books',
            data: [20,21],
            backgroundColor:'#15647D' 
            ,
            borderColor: '#9DBCC7',
            borderWidth: 2,
            padding:5
            
        }]
    },
    options: {
        scales: {  
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});