var ctx = document.getElementById('bar').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Borrowed', 'Returned'],
        datasets: [{
            label:'Borrowed And Returned',
            data: [24,16],
            backgroundColor:['#15647D','#B72424']
            ,
            borderColor: '#9DBCC7',
            borderWidth: 2,
            padding:5
            
        }]
    },
    options: {
        scales: {  
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});




var ctx = document.getElementById('pie').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['Patron', 'Popular books'],
        datasets: [{
            data: [21,3],
            backgroundColor:['#15647D','#B72424' ],
            borderColor: '#9DBCC7',
            borderWidth: 1,
            padding:5
            
        }]
    },
    options: {
        scales: {  
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

